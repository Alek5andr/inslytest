import ModalWindow from '@/pageObjects/components/modalWindows/modalWindow.js';

class PrivacyPolicy extends ModalWindow.classDefinition {
    constructor() {
        super();

        this.scrollDownArrowXoffsetInPercentage = 0.9611111111111111;
        this.scrollDownArrowYoffsetInPercentage = 0.9454855195911414;
    }

    scrollDownPrivacyPolicy(title = "Privacy Policy") {
        super.leftMouseClickInModalWindow(title, this.scrollDownArrowXoffsetInPercentage, this.scrollDownArrowYoffsetInPercentage, 75);
    }
}

module.exports = new PrivacyPolicy();