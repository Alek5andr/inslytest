import Shared from '@/pageObjects/components/shared.js';

class Checkbox extends Shared {
    constructor() {
        super();

        this.locators = {
            checkbox: 'span[class^="icon-check-empty"]'
        }
    }

    checkCheckbox(parentElement, label) {
        return super.clickOnElement(super.returnRootElement(parentElement, label).$(this.locators.checkbox));
    }

    isCheckboxChecked(parentElement, label) {
        return super.returnRootElement(parentElement, label).$(this.locators.checkbox).isEnabled();
    }
}

module.exports = new Checkbox();