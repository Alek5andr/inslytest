import Shared from '@/pageObjects/components/shared.js';

class InputField extends Shared {
    constructor() {
        super();

        this.locators = {
            inputField: 'input'
        };

        this.attributes = {
          value: 'value'
        };
    }

    get inputElement() {
        return $(this.locators.inputField);
    }

    setValueToField(text, element) {
        (element || browser).$(this.locators.inputField).setValue(text);
    }

    readValueFromField(label, element) {
        return label == null ? element.$(this.locators.inputField).getAttribute(this.attributes.value) : super.returnRootElement(element, label).$(this.locators.inputField).getAttribute(this.attributes.value);
    }
}

module.exports = new InputField();