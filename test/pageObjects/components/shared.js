function noElementIsFound(text) {
    throw new Error("No element with this text is found: " + text);
}

const locators = {
    h1Title: 'h1',
    okIcon: 'span[class="icon-ok"]',
    anchor: 'a',
    bold: 'b'
}

class Shared {
    constructor() {
        this.TIME_MS = 3000;
    }

    get sharedLocators() {
        return locators;
    }

    get title() {
        return browser.getTitle();
    }

    get h1Element() {
        return $(locators.h1Title);
    }

    get aElement() {
        return $(locators.anchor);
    }

    get bElement() {
        return $(locators.bold);
    }

    get currentURL() {
        return browser.getUrl();
    }

    clickOnElement(element) {
        const isVisible = this.isElementVisible(element);
        if (isVisible) {
            element.scroll();
            element.click();
        }
        return isVisible;
    }

    returnRootElement(rootElements, text) {
        //console.log(rootElements.length); //TODO for debugging
        for (let i = 0; i < rootElements.length; i++) {
            //console.log(rootElements[i].getText()); //TODO for debugging
            if (rootElements[i].getText().includes(text)) {
                //console.log("Returning root element"); //TODO for debugging
                return rootElements[i];
            }
        }
    }

    isOKIconPresent(parentElement, label) {
        return (parentElement == null) ? this.isElementVisible(browser.$(locators.okIcon)) : this.isElementVisible(this.returnRootElement(parentElement, label).$(locators.okIcon));
    }

    isElementVisible(element, reverse = false) {
        element.waitForVisible(this.TIME_MS, reverse);
        if (element.isVisible()) {
            return true;
        }
        return false;
    }

    openLink(anchorText, element) {
        return this.clickOnElement((element || browser).$(`${locators.anchor}=${anchorText}`));
    }

    scrollWindowDownSomePixels(pixels) {
        browser.scroll(0, pixels);
    }

    isElementEnabled(element) {
        element.waitForEnabled(this.TIME_MS);
        return element.isEnabled();
    }

    waitForElementToDisappear(element, timeMS) {
        if (element.waitForExist(this.TIME_MS + 7000)) {
            element.waitForExist(timeMS, true);
        }
        return !element.isExisting();
    }
}

module.exports = Shared;