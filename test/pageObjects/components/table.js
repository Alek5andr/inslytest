import inputField from '@/pageObjects/components/inputField.js';
import Shared from '@/pageObjects/components/shared.js';
import singleDropdownList from '@/pageObjects/components/dropdownLists/singleDropdownList.js';
import checkbox from '@/pageObjects/components/checkbox.js';

class Table extends Shared {
    constructor() {
        super();

        this.locators = {
            body: 'tbody',
            td: 'td',
            subtitle: 'td[class="subtitle"]',
            labelClass: 'div[class="label"]',
            tr: 'tr',
            checklist: 'div[class="checklist"]',
            labelTag: 'label'
        };
    }

    get tableElement() {
        return $(this.locators.body);
    }

    get labelElement() {
        return this.tableElement.$(this.locators.labelClass);
    }

    get trElements() {
        return this.tableElement.$$(this.locators.tr);
    }

    get checklistElement() {
        return $(this.locators.checklist);
    }

    get checkboxLabelElement() {
        return this.checklistElement.$$(this.locators.labelTag);
    }

    isSubtitleExist(text) {
        const subtitles = this.tableElement.$$(this.locators.subtitle);
        for (let i = 0; i < subtitles.length; i++) {
            if (subtitles[i].getText() === text) {
                return true;
            }
        }
        return false;
    }

    setValueToTableCell(value, cellTitle) {
        const element = super.returnRootElement(this.trElements, cellTitle);
        inputField.setValueToField(value, element);
    }

    selectOptionFromDropdownListInTable(label, defaultOption, option) {
        singleDropdownList.selectOptionFromDropdownList(this.trElements, label, defaultOption, option);
    }

    readValueFromFieldInTable(label) {
        return inputField.readValueFromField(label, this.trElements);
    }

    isOKIconPresentInTable(label) {
        return super.isOKIconPresent(this.trElements, label);
    }

    openLinkInTable(cellTitle, linkText) {
        return super.openLink(linkText, super.returnRootElement(this.trElements, cellTitle));
    }

    checkCheckboxInTable(label) {
        return checkbox.checkCheckbox(this.checkboxLabelElement, label);
    }

    isCheckboxCheckedInTable(label) {
        return checkbox.isCheckboxChecked(this.checkboxLabelElement, label);
    }
}

module.exports = new Table();