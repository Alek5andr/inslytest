import Shared from '@/pageObjects/components/shared.js';

class Button extends Shared {
    constructor() {
        super();

        this.locators = {
            button: 'button'
        };
    }

    clickOnButton(text, element) {
        return super.clickOnElement((element || browser).$(`${this.locators.button}=${text}`));
    }

    isButtonEnabled(text, element) {
        return super.isElementEnabled((element || browser).$(`${this.locators.button}=${text}`));
    }
}

module.exports = new Button();