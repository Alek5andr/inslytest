class JavaScriptHelper {
    constructor() {

        this.mouseCursorScript = "// Create mouse following image.\n" +
            "var seleniumFollowerImg = document.createElement(\"img\");\n" +
            "\n" +
            "// Set image properties.\n" +
            "seleniumFollowerImg.setAttribute('src', 'data:image/png;base64,'\n" +
            "    + 'iVBORw0KGgoAAAANSUhEUgAAABQAAAAeCAQAAACGG/bgAAAAAmJLR0QA/4ePzL8AAAAJcEhZcwAA'\n" +
            "    + 'HsYAAB7GAZEt8iwAAAAHdElNRQfgAwgMIwdxU/i7AAABZklEQVQ4y43TsU4UURSH8W+XmYwkS2I0'\n" +
            "    + '9CRKpKGhsvIJjG9giQmliHFZlkUIGnEF7KTiCagpsYHWhoTQaiUUxLixYZb5KAAZZhbunu7O/PKf'\n" +
            "    + 'e+fcA+/pqwb4DuximEqXhT4iI8dMpBWEsWsuGYdpZFttiLSSgTvhZ1W/SvfO1CvYdV1kPghV68a3'\n" +
            "    + '0zzUWZH5pBqEui7dnqlFmLoq0gxC1XfGZdoLal2kea8ahLoqKXNAJQBT2yJzwUTVt0bS6ANqy1ga'\n" +
            "    + 'VCEq/oVTtjji4hQVhhnlYBH4WIJV9vlkXLm+10R8oJb79Jl1j9UdazJRGpkrmNkSF9SOz2T71s7M'\n" +
            "    + 'SIfD2lmmfjGSRz3hK8l4w1P+bah/HJLN0sys2JSMZQB+jKo6KSc8vLlLn5ikzF4268Wg2+pPOWW6'\n" +
            "    + 'ONcpr3PrXy9VfS473M/D7H+TLmrqsXtOGctvxvMv2oVNP+Av0uHbzbxyJaywyUjx8TlnPY2YxqkD'\n" +
            "    + 'dAAAAABJRU5ErkJggg==');\n" +
            "seleniumFollowerImg.setAttribute('id', 'selenium_mouse_follower');\n" +
            "seleniumFollowerImg.setAttribute('style', 'position: absolute; z-index: 99999999999; pointer-events: none;');\n" +
            "\n" +
            "// Add mouse follower to the web page.\n" +
            "document.body.appendChild(seleniumFollowerImg);\n" +
            "\n" +
            "// Track mouse movements and re-position the mouse follower.\n" +
            "$(document).mousemove(function(e) {\n" +
            "    $(\"#selenium_mouse_follower\").css({ left: e.pageX, top: e.pageY });\n" +
            "});";
    }

    showMouseCursor() {
        browser.execute(this.mouseCursorScript);
    }
}

module.exports = new JavaScriptHelper();