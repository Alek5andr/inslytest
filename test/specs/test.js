import testData from '~/testData.json';
import table from '@/pageObjects/components/table.js';
import modalWindow from '@/pageObjects/components/modalWindows/modalWindow.js';
import privacyPolicy from '@/pageObjects/privacyPolicy.js';
import button from '@/pageObjects/components/button.js';
import instanceCreation from '@/pageObjects/instanceCreation.js';
import dashboard from '@/pageObjects/Dashboard.js';

describe('Register an account. ', () => {
    it('Navigate to sign up form.', () => {
        expect(table.title).toBe("Insly");
        expect(table.h1Element.getText()).toBe(testData.titles.signUp);
        expect(table.isSubtitleExist(testData.table.titles.adminDetails)).toBe(true);
        expect(table.isSubtitleExist(testData.table.titles.termsAndConds)).toBe(true);
    });

    describe('Fill in sign up form. ', () => {
        var generatedPassword = "";

        it('Fill in "Company" section.', () => {
            table.setValueToTableCell(testData.signUpForm.userInput.company.name, testData.signUpForm.labels.company.name);
            table.selectOptionFromDropdownListInTable(testData.signUpForm.labels.company.profile, testData.signUpForm.userInput.company.dropdownList.defaultOption, testData.signUpForm.userInput.company.dropdownList.profile);
            table.selectOptionFromDropdownListInTable(testData.signUpForm.labels.company.numOfEmployees, testData.signUpForm.userInput.company.dropdownList.defaultOption, testData.signUpForm.userInput.company.dropdownList.numOfEmployees);
            table.selectOptionFromDropdownListInTable(testData.signUpForm.labels.company.description, testData.signUpForm.userInput.company.dropdownList.defaultOption, testData.signUpForm.userInput.company.dropdownList.description);
            expect(table.readValueFromFieldInTable(testData.signUpForm.labels.company.address)).toEqual(testData.signUpForm.userInput.company.name);
            expect(table.isOKIconPresentInTable(testData.signUpForm.labels.company.address)).toBe(true);
        });

        it(`Fill in "${testData.table.titles.adminDetails}" section.`, () => {
            table.setValueToTableCell(testData.signUpForm.userInput.administration.email, testData.signUpForm.labels.administration.email);
            table.setValueToTableCell(testData.signUpForm.userInput.administration.managerName, testData.signUpForm.labels.administration.managerName);
            table.setValueToTableCell(testData.signUpForm.userInput.administration.phone, testData.signUpForm.labels.administration.phone);
            expect(table.openLinkInTable(testData.signUpForm.labels.administration.password, testData.signUpForm.links.password)).toBe(true);
            expect(modalWindow.isModalWindowOpen()).toBe(true);
            generatedPassword = modalWindow.readPassword();
            console.log("Generated password: " + generatedPassword);
            expect(button.clickOnButton(testData.buttons.modalWindow.ok)).toBe(true);
            expect(modalWindow.isModalWindowClosed()).toBe(true);
        });

        describe(`Check checkboxes in "${testData.table.titles.termsAndConds}" section. `, () => {
            it(`Check "${testData.signUpForm.labels.termsAndConds.agreeGDPR}".`, () => {
                expect(table.checkCheckboxInTable(testData.signUpForm.labels.termsAndConds.agreeGDPR)).toBe(true);
            });

            it(`Check "${testData.signUpForm.labels.termsAndConds.agreeTermsAndConds}".`, () => {
                /* TODO Bug within the system - accepting Terms&Conds from the modal window is not really checked.
                expect(table.openLink(testData.signUpForm.links.termsAndConds)).toBe(true);
                expect(modalWindow.isModalWindowOpen(testData.titles.termsAndConds)).toBe(true);
                expect(button.clickOnButton(testData.buttons.modalWindow.agree)).toBe(true);
                expect(modalWindow.isModalWindowClosed(testData.titles.termsAndConds)).toBe(true);
                expect(table.isCheckboxCheckedInTable(testData.signUpForm.labels.termsAndConds.agreeTermsAndConds)).toBe(true);
                */
                expect(table.checkCheckboxInTable(testData.signUpForm.labels.termsAndConds.agreeTermsAndConds)).toBe(true);
            });

            it(`Check "${testData.signUpForm.labels.termsAndConds.agreePrivacyPolicy}".`, () => {
                expect(table.openLink(testData.signUpForm.links.privatePolicy)).toBe(true);
                expect(modalWindow.isModalWindowOpen(testData.titles.privatePolicy)).toBe(true);
                privacyPolicy.scrollDownPrivacyPolicy();
                expect(modalWindow.closeModalWindow(testData.titles.privatePolicy)).toBe(true);
                expect(modalWindow.isModalWindowClosed(testData.titles.privatePolicy)).toBe(true);
                expect(table.checkCheckboxInTable(testData.signUpForm.labels.termsAndConds.agreePrivacyPolicy)).toBe(true);
            });
        });
    });

    describe("Submit form and check landing to Dashboard. ", () => {
        const userDashboard = "https://" + testData.signUpForm.userInput.company.name + ".insly.com/dashboard";

        it("Submit form.", () => {
            expect(button.isButtonEnabled(testData.buttons.signUp)).toBe(true);
            expect(button.clickOnButton(testData.buttons.signUp)).toBe(true);
        });

        it("Check landing to Dashboard.", () => {
            expect(instanceCreation.waitForClockIconToDisappear(testData.time.disappearTime)).toBe(true);
            expect(dashboard.currentURL).toEqual(userDashboard);
            expect(dashboard.loggedInUser).toEqual(testData.signUpForm.userInput.administration.managerName);
        });
    });
});