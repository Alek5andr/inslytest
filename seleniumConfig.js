module.exports = {
    drivers: {
        chrome: {
            version: '2.38',
            arch: process.arch,
            baseURL: 'https://chromedriver.storage.googleapis.com'
        },
    },
}